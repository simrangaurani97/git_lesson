def my_square(x):
        """ takes a value and returns a squared value.
        uses the ** operator
        """
        return(x ** 2)

def my_square2(x):
        return(x * x)

print(my_square(8))
print(my_square2(8))
